FreeIPA Client
==============
Installs FreeIPA and joins a realm

Supported Operating Systems
---------------------------
* CentOS 7

Requirements
------------
No Requirements

Role Variables
--------------
None

Role Defaults
-------------
None

Dependencies
------------
None

Configuring FreeIPA
-------------------
To join the FreeIPA realm, the following variables need to be set:

	dm_password: <string> the domain admin password

License
-------
ISC

Author Information
------------------
CV Admins <cvadmins@utdallas.edu>
